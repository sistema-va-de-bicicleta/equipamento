package com.pm.maven.eclipse;

import io.javalin.Javalin;

public class HelloWorld {
    public static void main2(String[] args) {
        Javalin app = Javalin.create().start(8000);
        app.get("/", ctx -> ctx.result("Hello World"));
    }
}