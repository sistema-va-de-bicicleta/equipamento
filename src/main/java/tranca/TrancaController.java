package tranca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import bicicleta.Bicicleta;
import bicicleta.BicicletaController;
import email.EmailController;
import io.javalin.http.Context;
import kong.unirest.json.JSONObject;

public class TrancaController {
	
	public static ArrayList<Tranca> trancas = new ArrayList<>(Arrays.asList(new Tranca(22, "2.00, 7.00", "2020", "yyy", StatusT.LIVRE)));
	public static ArrayList<Bicicleta> bicicletas = BicicletaController.bicicletas;
	
	private final static String TRANCA_ID = "idTranca";
	private final static String DADOS_INVALIDOS = "Dados Invalidos";
	private final static String NAO_ENCONTRADO = "Não Encontrado";
	
	private TrancaController() {
		throw new IllegalStateException("TrancaController");
	}
	
	public static void integrandoRede(Context ctx) {
		
		String tra = ctx.body();
		JSONObject json = new JSONObject(tra);
		
		String idTranca = json.getString(TRANCA_ID);	
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					if (tranca.getStatus().equals("NOVA") || tranca.getStatus().equals("EM_REPARO")) {
						
						tranca.setStatus(StatusT.LIVRE);
						EmailController.enviarEmailTrancaReparador(tranca);
						ctx.status(200);
						
					}else {throw new IllegalArgumentException();}
				}
			}
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		

	}
	
	public static void retirandoRede(Context ctx) {
		
		String tra = ctx.body();
		JSONObject json = new JSONObject(tra);
		
		String idTranca = json.getString(TRANCA_ID);
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					if (tranca.getStatus().equals("APOSENTADA") || 
						tranca.getStatus().equals("EM_REPARO") ||
						tranca.getBicicleta() != null) {
						
						throw new IllegalArgumentException();	
						
					}else {
						tranca.setStatus(StatusT.EM_REPARO);
						
						EmailController.enviarEmailTrancaReparador(tranca);
						
						ctx.status(200);
					}
				}
			}
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		

	}
	
	public static void recuperaTrancas(Context ctx) {
		
		ctx.json(trancas);
	    ctx.status(200);
	    
	}
	
	public static void cadastraTranca(Context ctx) {
		
		try {
		
			String tra = ctx.body();
			JSONObject json = new JSONObject(tra);
			
			StatusT status = StatusT.NOVA;
			
			Tranca tranca = new Tranca
					(
					 json.getInt("numero"), 
					 json.getString("localizacao"), 
					 json.getString("anoDeFabricacao"), 
					 json.getString("modelo"),
					 status
					);
			
			trancas.add(tranca);
			
			ctx.status(200);
			ctx.json(tranca);
			
		} catch (IllegalArgumentException e) {
	    	ctx.html(DADOS_INVALIDOS);
	        ctx.status(422);
		}
					
	}
	
	public static void getTranca(Context ctx) {
		
		String idTranca = Objects.requireNonNull(ctx.pathParam(TRANCA_ID));
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
		        	ctx.status(200);
		        	ctx.json(tranca);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		}
		

	}
	
	public static void putTranca(Context ctx) {
		
		try {
			
			String idTranca = Objects.requireNonNull(ctx.pathParam(TRANCA_ID));
			
			String tra = ctx.body();
			JSONObject json = new JSONObject(tra);
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					StatusT status = StatusT.valueOf(json.getString("status"));
					
					tranca.setNumero(json.getInt("numero"));
					tranca.setLocalizacao(json.getString("localizacao"));
					tranca.setAnoFabricacao(json.getString("anoDeFabricacao"));
					tranca.setModelo(json.getString("modelo"));
					tranca.setStatus(status);
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(tranca);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
            
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
		}    
		

	}
	
	public static void deleteTranca(Context ctx) {
		
		String idTranca = Objects.requireNonNull(ctx.pathParam(TRANCA_ID));
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				
				if (trancas.get(i).getId().equals(idTranca)) {
					
					if (trancas.get(i).getBicicleta() == null) {
						
						trancas.remove(trancas.get(i));
			        	ctx.status(200);
			        	
					} else {throw new IllegalArgumentException();}	        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		} catch (IllegalArgumentException e) {
			ctx.html(DADOS_INVALIDOS);
			ctx.status(422);
		}

	}
	
	public static void getBicicletaTranca(Context ctx) {
		
		String idTranca = Objects.requireNonNull(ctx.pathParam(TRANCA_ID));
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				
				if (trancas.get(i).getId().equals(idTranca)) {
					Tranca tranca = trancas.get(i);
					
					for (int j = 0; j < bicicletas.size(); j++) {
						
						Bicicleta bicicleta = bicicletas.get(j);
						
						if(tranca.getBicicleta().equals(bicicleta.getId())){
				        	ctx.status(200);
				        	ctx.json(bicicleta);
						}
					}
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		} catch (Exception e) {
        	ctx.html("Id da Tranca inválido");
            ctx.status(422);
		}

	}
	
	
	public static void alteraStatus (Context ctx) {
		
		try {
			
			String idTranca = Objects.requireNonNull(ctx.pathParam(TRANCA_ID));
			
			trancas.get(0).setId(idTranca);
			
			String acao = Objects.requireNonNull(ctx.pathParam("acao"));
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					StatusT status = StatusT.valueOf(acao);
					
					tranca.setStatus(status);
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(tranca);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
            
		} catch (Exception e) {
        	ctx.html(DADOS_INVALIDOS);
		}  

	}	
	
}
