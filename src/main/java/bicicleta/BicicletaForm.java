package bicicleta;

import java.time.Year;
import java.util.UUID;

public class BicicletaForm {
	
	private String id;
	private String marca;
	private String modelo;
	private String ano;
	private String numero;
	private String status;
	
	public BicicletaForm() {
	}
	
	public BicicletaForm(String marca, String modelo, String ano, String numero) {
		this.marca = marca;
		this.modelo = modelo;
		this.ano = ano;
		this.numero = numero;
	}

	public Bicicleta deserializando()
	{
		int ano = Integer.parseInt(this.ano);
		Year year = Year.of(ano);
		
		return new Bicicleta(this.marca, this.modelo, year, Integer.parseInt(this.numero), Status.NOVA);
	}
	
}
