package bicicleta;

public enum Status {
	DISPONIVEL, EM_USO, NOVA, APOSENTADA, REPARO_SOLICITADO, EM_REPARO;
}
