package bicicleta;

import java.time.Year;

import java.util.ArrayList;
import java.util.Arrays;

import tranca.*;

import io.javalin.http.Context;
import kong.unirest.json.JSONObject;
import java.util.Objects;

import email.EmailController;

public class BicicletaController {
	
	public static ArrayList<Bicicleta> bicicletas = new ArrayList<Bicicleta>(Arrays.asList(new Bicicleta("caloi", "vulcan", Year.now(), 324, Status.NOVA)));
	public static ArrayList<Tranca> trancas = TrancaController.trancas;
	
	private final static String BIKE_ID = "idBicicleta";
	private final static String DADOS_INVALIDOS = "Dados Invalidos";
	private final static String NAO_ENCONTRADO = "Nao Encontrado";
	

	private BicicletaController() {
		throw new IllegalStateException("BicicletaController");
	}
	
	
	public static void retornaBicicleta(Context ctx) {
		
		ctx.json(bicicletas);
	    ctx.status(200);
		

	}
	
	public static void cadastraBicicleta(Context ctx) {
		
		//Cadastrando Bike nova
		
		try {
			
			String bike = ctx.body();
			JSONObject json = new JSONObject(bike);
		
		
			int ano = Integer.parseInt(json.getString("ano"));
			Year year = Year.of(ano);
			
			Status status = Status.NOVA;
			
			Bicicleta bicicleta = new Bicicleta
					(
					 json.getString("marca"), 
					 json.getString("modelo"), 
					 year, 
					 json.getInt("numero"),
					 status
					);
			
			bicicletas.add(bicicleta);
			
			ctx.status(200);
			ctx.json(bicicleta);
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		

	}
	
	public static void integrandoRede(Context ctx) {
		
		String bike = ctx.body();
		JSONObject json = new JSONObject(bike);
		
		String idBicicleta = json.getString(BIKE_ID);
		String idTranca = json.getString("idTranca");
		
		bicicletas.get(0).setId(idBicicleta);
		trancas.get(0).setId(idTranca);
		
		try {
			
			Bicicleta bicicleta = checkNovaOuReparo(idBicicleta);
			
			if(bicicleta == null) {throw new IllegalArgumentException();}
			
			Tranca tranca = checkLivre(idTranca);
			
			if(tranca == null) {throw new IllegalArgumentException();}
			
			bicicleta.setStatus(Status.DISPONIVEL);
			tranca.setStatus(StatusT.OCUPADA);
			
			tranca.setBicicleta(bicicleta.getId());
			
			EmailController.enviarEmailBicicletaReparador(bicicleta, tranca);
			
			ctx.status(200);
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		

	}
	
	private static Bicicleta checkNovaOuReparo(String idBicicleta) {
	
		
		for (int i = 0; i < bicicletas.size(); i++) {
			if (bicicletas.get(i).getId().equals(idBicicleta)) {
				
				Bicicleta bicicleta = bicicletas.get(i);
				
				if (bicicleta.getStatus().equals("NOVA") || bicicleta.getStatus().equals("EM_REPARO")) {
					return bicicleta;
				}
	        	
			}
		}
		return null;
	}
	
	private static Tranca checkLivre(String idTranca) {
		
		for (int i = 0; i < trancas.size(); i++) {
			if (trancas.get(i).getId().equals(idTranca)) {
				
				Tranca tranca = trancas.get(i);
				
				if (tranca.getStatus().equals("LIVRE")) {
					return tranca;
				}
			}
		}
		
		return null;
	}
	
	public static void retirandoRede(Context ctx) {
		

		String bike = ctx.body();
		JSONObject json = new JSONObject(bike);
		
		String idBicicleta = json.getString(BIKE_ID);
		String idTranca = json.getString("idTranca");
		
		try {
			
			Tranca tranca = checkTranca(idTranca);
			
			if(tranca == null) {throw new IllegalArgumentException();}
			if(!tranca.getBicicleta().equals(idBicicleta)){throw new IllegalArgumentException();}
			
			Bicicleta bicicleta = checkBicicleta(idBicicleta);
			
			if(bicicleta == null) {throw new IllegalArgumentException();}
			
			if (bicicleta.getStatus().equals("REPARO_SOLICITADO") && tranca.getStatus().equals("OCUPADA")) {
				bicicleta.setStatus(Status.EM_REPARO);
				tranca.setStatus(StatusT.LIVRE);
				
				EmailController.enviarEmailBicicletaReparador(bicicleta, tranca);
				
				ctx.status(200);
			}
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		

	}
	
	private static Tranca checkTranca(String idTranca) {
		
		for (int i = 0; i < trancas.size(); i++) {
			if (trancas.get(i).getId().equals(idTranca)) {
				
				Tranca tranca = trancas.get(i);
				return tranca;
			}
		}
		return null;
	}
	
	private static Bicicleta checkBicicleta(String idBicicleta) {

		for (int i = 0; i < bicicletas.size(); i++) {
			if (bicicletas.get(i).getId().equals(idBicicleta)) {
				
				Bicicleta bicicleta = bicicletas.get(i);
				
				return bicicleta;
			}
		}
		return null;
	}
	
	public static void getBicicleta(Context ctx) {
		
		
		String bicicletaId = Objects.requireNonNull(ctx.pathParam(BIKE_ID));
		
		try {
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					Bicicleta bicicleta = bicicletas.get(i);
		        	ctx.status(200);
		        	ctx.json(bicicleta);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		}
		

	}
	
	public static void putBicicleta(Context ctx) {
		
		try {
			
			String bicicletaId = Objects.requireNonNull(ctx.pathParam(BIKE_ID));
			
			String bike = ctx.body();
			JSONObject json = new JSONObject(bike);
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					Bicicleta bicicleta = bicicletas.get(i);
					
					int ano = Integer.parseInt(json.getString("ano"));
					Year year = Year.of(ano);
										
					bicicleta.setMarca(json.getString("marca"));
					bicicleta.setModelo(json.getString("modelo"));
					bicicleta.setAno(year);
					bicicleta.setNumero(json.getInt("numero"));
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(bicicleta);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
            
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);		
		}

	}
	
	public static void deleteBicicleta(Context ctx) {
		
		
		String bicicletaId = Objects.requireNonNull(ctx.pathParam(BIKE_ID));
		
		try {
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					bicicletas.remove(bicicletas.get(i));
					
		        	ctx.status(200);
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		}		

	}
	
	public static void alteraStatus(Context ctx) {
		
	
		
		String bicicletaId = Objects.requireNonNull(ctx.pathParam("id"));
		bicicletas.get(0).setId(bicicletaId);
		
		String acao = Objects.requireNonNull(ctx.pathParam("acao"));
		
		try {
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					Bicicleta bicicleta = bicicletas.get(i);
					
					Status status = Status.valueOf(acao);
					
					bicicleta.setStatus(status);
					
		        	ctx.status(200);
		        	ctx.json(bicicleta);
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}

	}
}