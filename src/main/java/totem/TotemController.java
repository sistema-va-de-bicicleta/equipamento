package totem;

import java.util.ArrayList;
import java.util.Objects;

import tranca.*;
import bicicleta.*;

import io.javalin.http.Context;
import kong.unirest.json.JSONObject;

public class TotemController {
	
	public static ArrayList<Totem> totens = new ArrayList<Totem>();
	public static ArrayList<Tranca> trancas = TrancaController.trancas;
	public static ArrayList<Bicicleta> bicicletas = BicicletaController.bicicletas;
	
	private final static String TOTEM_ID = "idTotem";
	private final static String DADOS_INVALIDOS = "Dados Invalidos";
	private final static String NAO_ENCONTRADO = "Nao Encontrado";
	
	private TotemController() {
		throw new IllegalStateException("TotemController");
	}
	
	public static void recuperaTotens(Context ctx) {
		
		ctx.json(totens);
	    ctx.status(200);
		
	}
	
	public static void cadastraTotem(Context ctx) {
		
		try {
			
			String tot = ctx.body();
			JSONObject json = new JSONObject(tot);
			
			Totem totem = new Totem
					(
					 json.getString("localizacao")
					);
			
			totens.add(totem);
			
			ctx.status(200);
			ctx.json(totem);
			
		} catch (Exception e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		
	}
	
	public static void putTotem(Context ctx) {
		
		try {
			
			String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEM_ID));
			
			String tot = ctx.body();
			JSONObject json = new JSONObject(tot);
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					Totem totem = totens.get(i);
										
					totem.setLocalizacao(json.getString("localizacao"));
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(totem);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
            
		} catch (Exception e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);		
		}
		
	}
	
	public static void deleteTotem(Context ctx) {
		
		
		
		String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEM_ID));
		
		try {
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					totens.remove(totens.get(i));
					
		        	ctx.status(200);
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		}		

	}
	
	public static void getTrancasTotem(Context ctx) {
		
		
		String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEM_ID));
		ArrayList<Tranca> trancasTotem = new ArrayList<Tranca>();
		
		try {
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					Totem totem = totens.get(i);
					
					for (int j = 0; j < trancas.size(); j++) {
						if (trancas.get(j).getLocalizacao().equals(totem.getLocalizacao())) {
							
							trancasTotem.add(trancas.get(j));
						}
					}			
					
					ctx.status(200);
		        	ctx.json(trancasTotem);
					
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		
	}
	
	public static void getBicicletasTotem(Context ctx) {
		
		String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEM_ID));
		ArrayList<Bicicleta> bicicletasTotem = new ArrayList<Bicicleta>();
		Totem totem = null;
		Tranca tranca = null;
		
		try {
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					totem = totens.get(i);
					break;
				}
			}
			
			for (int i = 0; i < trancas.size(); i++) {
				
				tranca = trancas.get(i);
				
				if (tranca.getLocalizacao().equals(totem.getLocalizacao())) {
					
					for (int j = 0; j < bicicletas.size(); j++) {
						
						if (tranca.getBicicleta().equals(bicicletas.get(j).getId())) {
							bicicletasTotem.add(bicicletas.get(j));
						}
					}

				}
			}			
			
			ctx.status(200);
        	ctx.json(bicicletasTotem);
        	
		} catch (NullPointerException n) {
        	ctx.html(NAO_ENCONTRADO);
            ctx.status(404);
		} catch (Exception e) {
        	ctx.html(DADOS_INVALIDOS);
            ctx.status(422);
		}
		

	}
}
