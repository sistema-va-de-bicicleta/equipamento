package main.util;

import static io.javalin.apibuilder.ApiBuilder.*;

import bicicleta.BicicletaController;
import totem.TotemController;
import tranca.TrancaController;
import io.javalin.Javalin;

public class JavalinApp 
{
	private Javalin app =
			Javalin.create
			(
				config -> config.defaultContentType = "application/json")
            		.routes
            		(() -> 
	            		{
	            			path("/bicicleta",  ()-> get(BicicletaController::retornaBicicleta));
	            			
	            			path("/bicicleta",  ()-> post(BicicletaController::cadastraBicicleta));
	            			
	            			path("/bicicleta/integrandoRede",  () -> post(BicicletaController::integrandoRede));

	            			path("/bicicleta/retirandoRede", ()-> post(BicicletaController::retirandoRede));
	            			
	            			path("/bicicleta/:idBicicleta", ()-> get(BicicletaController::getBicicleta));
	            			
	            			path("/bicicleta/:idBicicleta", ()-> put(BicicletaController::putBicicleta));
	            			
	            			path("/bicicleta/:idBicicleta", ()-> delete(BicicletaController::deleteBicicleta));
	            			
	            			path("/bicicleta/:id/status/:acao", ()-> post(BicicletaController::alteraStatus));
	            			
	            			path("/totem",  ()-> get(TotemController::recuperaTotens));
	            			
	            			path("/totem",  ()-> post(TotemController::cadastraTotem));
	            			
	            			path("/tranca/integrandoRede",  () -> post(TrancaController::integrandoRede));

	            			path("/tranca/retirandoRede", ()-> post(TrancaController::retirandoRede));
	            			
	            			path("/totem/:idTotem", ()-> put(TotemController::putTotem));
	            			
	            			path("/totem/:idTotem", ()-> delete(TotemController::deleteTotem));
	            			
	            			path("/totem/:idTotem/trancas", ()-> get(TotemController::getTrancasTotem));
	            			
	            			path("/totem/:idTotem/bicicletas", ()-> get(TotemController::getBicicletasTotem));
	            			
	            			path("/tranca",  ()-> get(TrancaController::recuperaTrancas));
	            			
	            			path("/tranca",  ()-> post(TrancaController::cadastraTranca));
	            			
	            			path("/tranca/:idTranca",  ()-> get(TrancaController::getTranca));
	            			
	            			path("/tranca/:idTranca",  ()-> put(TrancaController::putTranca));
	            		
	            			path("/tranca/:idTranca",  ()-> delete(TrancaController::deleteTranca));
	            			
	            			path("/tranca/:idTranca/bicicleta",  ()-> get(TrancaController::getBicicletaTranca));
	            			
	            			path("/tranca/:idTranca/status/:acao",  ()-> post(TrancaController::alteraStatus));
	            		
	            		}
            		);
	
	public void start(int port) 
	{
        this.app.start(port);
    }

    public void stop() 
    {
        this.app.stop();
    }
}
